import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import pandas as pd
import base64

from dash.dependencies import Input, Output

df = pd.read_csv('./wheels.csv')

app = dash.Dash()


def encode_image(image_file):
    encoded = base64.b64encode(open(image_file, 'rb').read())
    return f'data:image/png;base64,{encoded.decode()}'


app.layout = html.Div(
    [
        html.Div(
            dcc.RadioItems(
                id='wheels',
                options=[
                    {'label':i, 'value':i} for i in df.wheels.unique()
                ],
                value=1
            )
        ),
        html.Div(id='wheels_output'),
        html.Hr(),
        html.Div(
            dcc.RadioItems(
                id='colours',
                options=[
                    {'label':i, 'value':i} for i in df.color.unique()
                ],
                value=df.iloc[0].color
            )
        ),
        html.Div(id='colours_output'),
        html.Img(id='display_image', src='children', height=300)
    ],
    style={'fontFamily':'helvetica', 'fontSize':18}
)


@app.callback(
    Output('wheels_output', 'children'),
    [
        Input('wheels', 'value')
    ]
)
def callback_a(wheels_value):
    return f"you chose {wheels_value}"


@app.callback(
    Output('colours_output', 'children'),
    [
        Input('colours', 'value')
    ]
)
def callback_b(colours_value):
    return f"you chose {colours_value}"


@app.callback(
    Output('display_image', 'src'),
    [
        Input('wheels', 'value'),
        Input('colours', 'value')
    ]
)
def callback_image(wheels, colour):
    path = df[(df['wheels'] == wheels) & (df['color']==colour)]['image'].values[0]
    return encode_image(
        './Images/' +
        str(path)
    )


if __name__ == "__main__":
    app.run_server()
