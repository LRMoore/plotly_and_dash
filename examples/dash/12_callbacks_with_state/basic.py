import dash
import dash_core_components as dcc
import dash_html_components as html

from dash.dependencies import Input, Output, State

app = dash.Dash()

app.layout = html.Div(
    children=[
        dcc.Input(id='number_in', value=1, style={'fontSize':24}),
        html.Button(
            id='submit_button',
            n_clicks=0,
            children='Submit Here',
            style={'fontSize':24}
        ),
        html.H1(id='number_out')
    ]
)


@app.callback(
    Output('number_out', 'children'),
    [
        Input('submit_button', 'n_clicks')
    ],
    [
        State('number_in', 'value')
    ]
)
def output(n_clicks, number):
    """ gets fed input and state params """
    return f"{number} was typed in, button was clicked {n_clicks} times"


if __name__ == "__main__":
    app.run_server()
