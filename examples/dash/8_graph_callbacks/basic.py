import dash
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
import plotly.graph_objs as go

from dash.dependencies import Input, Output


df = pd.read_csv('./gapminderDataFiveYear.csv')

year_options = []
for year in df.year.unique():
    year_options.append({'label':str(year), 'value':year})

app = dash.Dash()

app.layout = html.Div(
    [
        dcc.Graph(
            id='graph'
        ),
        dcc.Dropdown(
            id='year_picker',
            options=year_options,
            value=df.year.min()
        )
    ]
)

@app.callback(
    # component_id, component_property
    Output('graph', 'figure'),
    [Input('year_picker', 'value')]
)
def update_figure(selected_year):
    # data only for selected year from dropdown
    filtered_df = df[df['year'] == selected_year]
    traces = []
    for continent in filtered_df['continent'].unique():
        df_by_continent = filtered_df[filtered_df['continent'] == continent]
        traces.append(
            go.Scatter(x=df_by_continent.gdpPercap,
                       y=df_by_continent.lifeExp,
                       text=df_by_continent.country,
                       mode='markers',
                       marker=dict(
                          size=15
                       ),
                       opacity=0.7,
                       name=continent)
        )
    return {
        'data':traces,
        'layout':go.Layout(
            title='GDP per capita against life expectancy',
            xaxis={'title':'GDP per cap', 'type':'log'},
            yaxis={'title':'life exp'},
            hovermode='closest'
        )
    }


if __name__ == "__main__":
    app.run_server()
