import dash
import dash_core_components as dcc
import dash_html_components as html

from dash.dependencies import Input, Output


app = dash.Dash()



app.layout = html.Div(
    [
        html.H1(id='live-update-text'),
        dcc.Interval(
            id='interval-component',
            interval=2000,
            n_intervals=0
        )
    ]
)


@app.callback(
    Output('live-update-text', 'children'),
    [
        Input('interval-component', 'n_intervals')
    ]
)
def update_layout(n):
    return f"Crash free for {n} intervals"



if __name__ == "__main__":
    app.run_server()
