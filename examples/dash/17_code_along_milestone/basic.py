import pandas as pd
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import numpy as np
import pandas as pd
import pandas_datareader.data as web
import os

from datetime import datetime
from dash.dependencies import Input, Output, State


os.environ["IEX_API_KEY"] = 'pk_5545a0a21bc446519e1ef47d9f96a68e'

nsdq = pd.read_csv('NASDAQcompanylist.csv')
nsdq.set_index('Symbol', inplace=True)
options = []

for tic in nsdq.index:
    # {'label':'user sees', 'value':'script sees'}
    mydict = {}
    mydict['label'] = str(nsdq.loc[tic]["Name"]) + ' ' + tic
    mydict['value'] = tic
    options.append(mydict)


app = dash.Dash()

app.layout = html.Div(
    [
        html.H1('Stock ticker dashboard'),
        html.Div(
            [
                html.H3('Enter a ticker symbol:', style={'paddingRight':'30px'}),
                dcc.Dropdown(
                    id='my-ticker-symbol',
                    options=options,
                    value=['TSLA'],
                    multi=True,
                    style={'fontSize':24}
                ),
            ],
            style={'display':'inline-block',
                   'verticalAlign':'top',
                   'width':'30%'}
        ),
        html.Div(
            [
                html.H3('Select a start and end date:'),
                dcc.DatePickerRange(
                    id='my-date-picker',
                    min_date_allowed=datetime(2016,1,1),
                    max_date_allowed=datetime.today(),
                    start_date=datetime(2018,1,1),
                    end_date=datetime.today()
                )
            ],
            style={'display':'inline-block'}
        ),
        html.Div(
            [
                html.Button(
                    id='submit-button',
                    n_clicks=0,
                    children='Submit',
                    style={'fontSize':24, 'marginLeft':'30px'}
                )
            ],
            style={'display':'inline-block'}
        ),
        dcc.Graph(
            id='my-graph',
            figure={
                'data':[
                    {'x':[1,2], 'y':[3,1]}
                ],
                'layout':{'title':'Default Title'}
            },
        )
    ]
)


@app.callback(
    Output('my-graph', 'figure'),
    [
        Input('submit-button', 'n_clicks')
    ],
    [
        State('my-ticker-symbol', 'value'),
        State('my-date-picker', 'start_date'),
        State('my-date-picker', 'end_date')
    ]
)
def update_graph(n_clicks, ticker, start, end):
    start = datetime.strptime(start[:10], '%Y-%m-%d')
    end = datetime.strptime(end[:10], '%Y-%m-%d')
    traces = []
    for tic in ticker:
        df = web.DataReader(ticker, 'iex', start, end)
        traces.append({'x':df.index, 'y':df.close, 'name':tic})
    fig = {
        'data':traces,
        'layout':{'title':ticker}
    }
    return fig



if __name__ == "__main__":
    app.run_server()
