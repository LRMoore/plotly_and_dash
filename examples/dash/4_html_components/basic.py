import dash
import dash_html_components as html

app = dash.Dash()

app.layout = html.Div(
    # list here because div has multiple components!
    [
        'This is the outermost div',
        html.Div(
            # can have list wrapping or not if single item div
            ['This is an inner div'],
            style=dict(color='red', border='2px red solid')
        ),
        html.Div(
            ['This is yet another inner div'],
            style=dict(color='blue', border='3px blue solid')
        )
    ],
    style=dict(color='green', border='2px green solid')
)

if __name__ == "__main__":
    app.run_server()
