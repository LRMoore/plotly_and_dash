import pandas as pd
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import numpy as np

from dash.dependencies import Input, Output

df = pd.read_csv('./mpg.csv')
df['year'] = np.random.randint(-4, 5, df.shape[0])*.1 + df['model_year']


app = dash.Dash()

app.layout = html.Div(
    [
        html.Div([
            dcc.Graph(
                id='mpg-scatter',
                figure={
                    'data':[
                        go.Scatter(
                            x=df.year+1900,
                            y=df.mpg,
                            text=df['name'],
                            hoverinfo='text',
                            mode='markers',
                            marker={'size':8}
                        )
                    ],
                    'layout':go.Layout(
                        title='MPG data',
                        xaxis={'title':'Model Year'},
                        yaxis={'title':'MPG'},
                        hovermode='closest'
                    )
                }
            )],
            style={'width':'50%', 'display':'inline-block'}
        ),
        html.Div([
            dcc.Graph(
                id='mpg-line',
                figure={
                    'data':[
                        go.Scatter(x=[0,1],
                                   y=[0,1],
                                   mode='lines')
                    ],
                    'layout': go.Layout(title='Acceleration',
                                        margin={'l':0})
                }
            )
        ], style={'width':'20%', 'height':'50%', 'display':'inline-block'}),
        html.Div(
            [
                dcc.Markdown(id='mpg-stats')
            ],
            style={'width':'20%', 'height':'50%', 'display':'inline-block'}
        )
    ]
)


@app.callback(
    Output('mpg-line', 'figure'),
    [
        Input('mpg-scatter', 'hoverData')
    ]
)
def callback_graph(hover_data):
    v_index = hover_data['points'][0]['pointIndex']
    figure = {
        'data':[
            go.Scatter(x=[0,1],
                       y=[0, 60/df.iloc[v_index]['acceleration']],
                       mode='lines',
                       line={'width':3*df.iloc[v_index]['cylinders']})
        ],
        'layout':go.Layout(title=df.iloc[v_index]['name'],
                           xaxis={'visible':False},
                           yaxis={
                              'visible':False,
                              'range':[0, 60/df['acceleration'].min()]
                           },
                           margin={'l':0},
                           height=300)
    }
    return figure


@app.callback(
    Output('mpg-stats', 'children'),
    [
        Input('mpg-scatter', 'hoverData')
    ]
)
def callback_stats(hover_data):
    v_index = hover_data['points'][0]['pointIndex']
    stats = """
            {} cylinders\n
            {}cc displacement\n
            0 to 60mph in {} seconds
            """.format(
        df.iloc[v_index]['cylinders'],
        df.iloc[v_index]['displacement'],
        df.iloc[v_index]['acceleration']
    )
    return stats


if __name__ == "__main__":
    app.run_server()
