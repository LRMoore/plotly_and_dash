import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import pandas as pd
import numpy as np

app = dash.Dash()

# D: date, X: duration (m), Y: waiting time (m)
df = pd.read_csv('./OldFaithful.csv')
print(df)

app.layout = html.Div(
    children=[
        dcc.Graph(
            id='main_scatter',
            figure={
                'data' : [
                    go.Scatter(
                        x=df.X,
                        y=df.Y,
                        mode='markers',
                        marker=dict(
                            size=12,
                            color='rgb(50, 100, 150)',
                            symbol='pentagon',
                            line=dict(
                                width=2
                            )
                        )
                    )
                ],
                'layout': go.Layout(
                    title='Old Faithful eruption intervals vs durations',
                    xaxis=dict(title='duration (m)'),
                    yaxis=dict(title='interval (m)')
                )
            }
        )
    ]
)

if __name__ == "__main__":
    app.run_server()
