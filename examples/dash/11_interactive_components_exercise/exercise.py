import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go

from dash.dependencies import Input, Output

app = dash.Dash()


app.layout = html.Div(
    children=[
        dcc.RangeSlider(
            id='slider',
            min=-15,
            max=15,
            marks={i: str(i) for i in range(-15, 16)},
            step=1,
            value=[-5, 5]
        ),
        html.Br(),
        html.Hr(),
        html.Div(id='slider-product')
    ]
)


@app.callback(
    Output('slider-product', 'children'),
    [
        Input('slider', 'value')
    ]
)
def product(values):
    return f"Product = {values[0]*values[1]}"


if __name__ == "__main__":
    app.run_server()
