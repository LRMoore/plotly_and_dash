import dash
import dash_core_components as dcc
import dash_html_components as html

app = dash.Dash()

markdown_text = """
### Dash and Markdown

Dash apps can be written in Markdown
Dash uses the [CommonMark](http://commonmark.org) specification of Markdown.
Check out their [60-second Markdown Tutorial](http://commonmark.org/help/)
if this is your first introduction to Markdown.
MD includes syntax for things like **bold text** and *italics*,
inline `code` snippets, lists, quotes and more.
"""

app.layout = html.Div(children=[
    dcc.Markdown(children=markdown_text)
])

if __name__ == "__main__":
    app.run_server()
