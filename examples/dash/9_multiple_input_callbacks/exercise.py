import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import pandas as pd

from dash.dependencies import Input, Output

df = pd.read_csv('./mpg.csv')


app = dash.Dash()
# ['mpg', 'horsepower', ...]
features = df.columns

app.layout = html.Div(
    [
        html.Div(
            dcc.Dropdown(
                id='xaxis',
                options=[{'label':i , 'value':i} for i in features],
                value='displacement'
            ),
            style={'width':'48%', 'display':'inline-block'}
        ),
        html.Div(
            dcc.Dropdown(
                id='yaxis',
                options=[{'label':i , 'value':i} for i in features],
                value='mpg'
            ),
            style={'width':'48%', 'display':'inline-block'}
        ),
        dcc.Graph(id='feature_graphic')
    ],
    style={'padding':10}
)


@app.callback(
    Output('feature_graphic', 'figure'),
    [
        Input('xaxis', 'value'),
        Input('yaxis', 'value')
    ]
)
def update_graph(xaxis_name, yaxis_name):
    data = [
        go.Scatter(
            x=df[xaxis_name],
            y=df[yaxis_name],
            text=df['name'],
            mode='markers',
            marker=dict(
                size=15,
                symbol='pentagon',
                opacity=0.5,
                line={'width':0.5, 'color':'white'}
            ),
            opacity=0.7
        )
    ]
    layout = go.Layout(
        title='My dashboard for MPG',
        xaxis={'title':xaxis_name},
        yaxis={'title':yaxis_name},
        hovermode='closest'
    )
    return {'data':data, 'layout':layout}


if __name__ == "__main__":
    app.run_server()
