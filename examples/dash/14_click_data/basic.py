import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import pandas as pd
import json
import base64

from dash.dependencies import Input, Output

def encode_image(image_file):
    enc = base64.b64encode(open(image_file, 'rb').read())
    return f'data:image/png;base64,{enc.decode()}'


df = pd.read_csv('./wheels.csv')

app = dash.Dash()

app.layout = html.Div(
    [
        html.Div(
            dcc.Graph(
                id='wheels_plot',
                figure={
                    'data':[
                        go.Scatter(
                            x=df['color'],
                            y=df['wheels'],
                            dy=1,
                            mode='markers',
                            marker = {'size':15}
                        )
                    ],
                    'layout':go.Layout(
                        title='test dashbrd',
                        hovermode='closest'
                    )
                }
            ),
            style={'width':'30%', 'float':'left'}
        ),
        html.Div(
            html.Img(
                id='hover_image',
                src='children',
                height=300
            ),
            style={'width':'30%', 'paddingTop':35}
        )
    ]
)


@app.callback(
    Output('hover_image', 'src'),
    [
        Input('wheels_plot', 'clickData')
    ]
)
def callback_image(hover_data):
    wheel = hover_data['points'][0]['y']
    color = hover_data['points'][0]['x']
    img_dir = './Images/'
    path = (
        img_dir + df[
            (df['wheels']==wheel) & (df['color']==color)
        ]['image'].values[0]
    )
    return encode_image(
        path
    )


if __name__ == "__main__":
    app.run_server()
