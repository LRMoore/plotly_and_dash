import dash
import dash_core_components as dcc
import dash_html_components as html

app = dash.Dash()

colours = {
    'background':'#111111',
    'text':'#7FDBFF'
}

app.layout = html.Div(children=[
    html.H1("Hello Dash!",
            style={
                'textAlign':'center',
                'color':colours['text']
            }
    ),
    dcc.Graph(
        id='example',
        figure={
            'data': [
                {'x':[1,2,3], 'y':[4,1,2], 'type':'bar', 'name':'SF'},
                {'x':[1,2,3], 'y':[2,4,5], 'type':'bar', 'name':'NYC'}
            ],
            'layout': {
                'title':'BAR PLOTS!',
                'plot_bgcolor':colours['background'],
                'paper_bgcolor':colours['background'],
                'font':{'color': colours['text']}
            }
        }
    )
],
style={'backgroundColor':colours['background']}
)

if __name__ == "__main__":
    app.run_server()
