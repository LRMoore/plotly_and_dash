import plotly.offline as pyo
import plotly.graph_objs as go
import pandas as pd

snodgrass = [.209,.205,.196,.210,.202,.207,.224,.223,.220,.201]
twain = [.225,.262,.217,.240,.230,.229,.235,.217]

data = [go.Box(y=snodgrass,
               name='snodgrass',
               boxpoints='outliers',
               jitter=0., # artificially induced x-spread for readability
               pointpos=0.),
        go.Box(y=twain,
               name='twain')
]

pyo.plot(data)
