import plotly.offline as pyo
import plotly.graph_objs as go
import pandas as pd

df = pd.read_csv('abalone.csv')
print(df.head())

samples = df.rings.sample(20, replace=False)
rings_sample_1 = samples[:10]
rings_sample_2 = samples[10:]

data = [
    go.Box(
        y=rings_sample_1,
        name='sample 1',
        boxpoints='outliers'
    ),
    go.Box(
        y=rings_sample_2,
        name='sample 2',
        boxpoints='outliers'
    )
]

layout = go.Layout(title="Rings: two pop samples")

fig = go.Figure(data=data, layout=layout)

pyo.plot(fig)
