import plotly.offline as pyo
import plotly.graph_objs as go
import pandas as pd

df = pd.read_csv('2018WinterOlympics.csv')
print(df.head())

data = [go.Bar(x=df.NOC,
               y=df[col],
               name=col,
               marker={'color':rgb})
        for col, rgb in zip(
            ('Gold', 'Silver', 'Bronze'),
            ('#FFD700','#9EA0A1','#CD7F32')
        )
]
layout = go.Layout(title='Medals', barmode='stack')

fig = go.Figure(data=data, layout=layout)
pyo.plot(fig)
