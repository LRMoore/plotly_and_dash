import plotly.offline as pyo
import plotly.graph_objs as go
import pandas as pd

df = pd.read_csv('mocksurvey.csv', index_col=0)

print(df)

data = [go.Bar(y=df.index,
               x=df[response],
               name=response,
               marker={'color':rgb},
               orientation='h'
               )
        for response, rgb in zip(
            df.columns,
            ('red', 'green', 'blue', 'purple', 'orange')
        )
]

layout = go.Layout(title='question responses', barmode='stack')
fig = go.Figure(data=data, layout=layout)

pyo.plot(fig)
