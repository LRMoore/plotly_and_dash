import plotly.offline as pyo
# more complex plots: figure factory
import plotly.figure_factory as ff
import pandas as pd

df = pd.read_csv('iris.csv')
print(df.head())

hist_data = [
    df[df['class'] == c]['petal_length']
    for c in df['class'].unique()
]

fig = ff.create_distplot(
    hist_data=hist_data,
    group_labels=df['class'].unique(),
    bin_size=0.1
)

pyo.plot(fig)
