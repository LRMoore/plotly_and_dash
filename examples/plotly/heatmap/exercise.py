import plotly.offline as pyo
import plotly.graph_objs as go
import pandas as pd

from plotly import subplots

df = pd.read_csv('flights.csv')
print(df.head())

data = [
    go.Heatmap(
        x=df['year'],
        y=df['month'],
        z=df['passengers'],
        colorscale='Jet',
        zmin=100,
        zmax=650
    )
]

layout = go.Layout(title='Flights per month over the years')

fig = go.Figure(data=data, layout=layout)

pyo.plot(fig)
