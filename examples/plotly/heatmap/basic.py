import plotly.offline as pyo
#import plotly.figure_factory as ff
import plotly.graph_objs as go
import pandas as pd

from plotly import subplots


df_ca = pd.read_csv('2010SantaBarbaraCA.csv')
df_az = pd.read_csv('2010YumaAZ.csv')
df_ak = pd.read_csv('2010SitkaAK.csv')

data = [
    go.Heatmap(
        x=df['DAY'],
        y=df['LST_TIME'],
        z=df['T_HR_AVG'],
        colorscale='Jet',
        zmin=5,
        zmax=40
    )
    for df in (df_ca, df_az, df_ak)
]

fig = subplots.make_subplots(
    rows=1,
    cols=3,
    subplot_titles=['SB, CA', 'Yuma, AZ', 'Sitka, AK'],
    shared_yaxes=True
)

# insert the data in position row, col
fig.append_trace(data[0], 1, 1)
fig.append_trace(data[1], 1, 2)
fig.append_trace(data[2], 1, 3)

# can access layout object from figure like a dict
fig['layout'].update(title='Temps for 3 cities')

pyo.plot(fig)
