import numpy as np
import plotly.offline as pyo
import plotly.graph_objs as go

np.random.seed(56)

x_vals = np.linspace(0, 1., 100)
y_vals = np.random.randn(100)

# data
trace0 = go.Scatter(x=x_vals,
                    y=y_vals+5,
                    mode='markers',
                    name='markers')
trace1 = go.Scatter(x=x_vals,
                    y=y_vals,
                    mode='lines',
                    name='my_lines')
trace2 = go.Scatter(x=x_vals,
                    y=y_vals-5,
                    mode='lines+markers',
                    name='my_fav')
data = [trace0, trace1, trace2]

# layout
layout = go.Layout(title='Line Charts')

# plot
fig = go.Figure(data=data, layout=layout)
pyo.plot(fig)
