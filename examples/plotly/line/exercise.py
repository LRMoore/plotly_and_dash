import pandas as pd
import plotly.offline as pyo
import plotly.graph_objs as go

df = pd.read_csv('2010YumaAZ.csv')

data = [go.Scatter(x=df[df.DAY == d]['LST_TIME'],
                   y=df[df.DAY == d]['T_HR_AVG'],
                   mode='lines+markers',
                   marker=dict(
                       size=12,
                       symbol='pentagon',
                       line={'width':2}
                   ),
                   name=d)
        for d in df.DAY.unique()]

layout = go.Layout(title='Daily temperature',
                   xaxis=dict(
                       title='time',
                   ),
                   yaxis=dict(
                       title='temp'
                   ),
                   hovermode='closest'
)

pyo.plot(data)
#pyo.Layout()
